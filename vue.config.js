const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  publicPath: process.env.PUBLIC_PATH || "/",
  transpileDependencies: ["vuetify"],
    chainWebpack: config => {
      config.plugin("html").tap(args => {
        args[0].title = "GoBee - Administración";
        return args;
      });
    }
};
